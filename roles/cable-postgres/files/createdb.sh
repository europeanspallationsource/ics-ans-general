#!/bin/bash

psql -h localhost -p 5432 -U postgres postgres -c "CREATE USER cabledb_dev PASSWORD 'cabledb_dev'"
psql -h localhost -p 5432 -U postgres postgres -c "CREATE DATABASE cabledb_dev OWNER cabledb_dev"

sudo -u postgres psql -f /tmp/cabledb.sql
