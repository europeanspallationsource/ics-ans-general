#!/bin/bash

echo "This script will install Mantid"

cat > /etc/yum.repos.d/isis-rhel.repo << EOF
[isis-rhel]
name=ISIS Software Repository for Redhat Enterprise Linux \$releasever - \$basearch
baseurl=http://yum.isis.rl.ac.uk/rhel/\$releasever/\$basearch
failovermethod=priority
enabled=1
gpgcheck=0

[isis-rhel-noarch]
name=ISIS Software Repository for Redhat Enterprise Linux \$releasever - noarch
baseurl=http://yum.isis.rl.ac.uk/rhel/\$releasever/noarch
failovermethod=priority
enabled=1
gpgcheck=0

[isis-rhel-debuginfo]
name=ISIS Software Repository for Redhat Enterprise Linux \$releasever - \$basearch - Debug
baseurl=http://yum.isis.rl.ac.uk/rhel/\$releasever/\$basearch/debug
failovermethod=priority
enabled=1
gpgcheck=0

[isis-rhel-source]
name=ISIS Software Repository for Redhat Enterprise Linux \$releasever - \$basearch - Source
baseurl=http://yum.isis.rl.ac.uk/rhel/\$releasever/SRPMS
failovermethod=priority
enabled=0
gpgcheck=0
EOF

curl -o /etc/pki/rpm-gpg/RPM-GPG-KEY-ess_ics https://artifactory01.esss.lu.se/artifactory/list/devenv/repositories/devenv-extras/RPM-GPG-KEY-ess_ics

cat > /etc/yum.repos.d/epel-19012016.repo << EOF
[epel-19012016]
name=EPEL version 19012016
baseurl=https://rpm01.esss.lu.se/public/centos/epel-19012016
failovermethod=priority
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ess_ics

[epel-19012016-debuginfo]
name=EPEL debuginfo version 19012016
baseurl=https://rpm01.esss.lu.se/public/centos/epel-19012016/debug
failovermethod=priority
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ess_ics
gpgcheck=1
EOF

yum-config-manager --disable epel
yum-config-manager --enable epel-19012016

yum -y install OCE-devel OCE-foundation OCE-ocaf OCE-draw OCE-modeling OCE-visualization

yum-config-manager --disable epel-19012016
yum-config-manager --enable epel

yum -y install mantid
