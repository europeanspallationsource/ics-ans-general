# This is the configuration file for the LDAP nameservice
# switch library's nslcd daemon. It configures the mapping
# between NSS names (see /etc/nsswitch.conf) and LDAP
# information in the directory.
# See the manual page nslcd.conf(5) for more information.

# The user and group nslcd should run as.
uid nslcd
gid ldap

# The uri pointing to the LDAP server to use for name lookups.
# Multiple entries may be specified. The address that is used
# here should be resolvable without using LDAP (obviously).

uri ldaps://icsv-ldapproxy-01

# The LDAP version to use (defaults to 3
# if supported by client library)
#ldap_version 3

# The distinguished name of the search base.
base ou=ad,dc=local

# The distinguished name to bind to the server with.
# Optional: default is to bind anonymously.
#binddn cn=proxyuser,dc=example,dc=com

# NOTE: Anonymous bind is done between client and proxy

# The credentials to bind with.
# Optional: default is no credentials.
# Note that if you set a bindpw you should check the permissions of this file.
#bindpw secret

# The distinguished name to perform password modifications by root by.
#rootpwmoddn cn=admin,dc=example,dc=com

# The default search scope.
#scope sub
#scope one
#scope base

# Customize certain database lookups.
# TODO: Have an ESS LDAP/AD expert look into this
#base   group  ou=Groups,dc=example,dc=com
#base   passwd ou=People,dc=example,dc=com
#base   shadow ou=People,dc=example,dc=com
#scope  group  onelevel
#scope  hosts  sub

# Bind/connect timelimit.
#bind_timelimit 30

# Search timelimit.
#timelimit 30

# Idle timelimit. nslcd will close connections if the
# server has not been contacted for the number of seconds.
#idle_timelimit 3600

# Use TLS without verifying the server certificate.
ssl on

# FIXME: Is it necessary to verify the certificate of the LDAP server?
#        In that case, install the self-signed root CA cert that currently
#        signs the certificates for the ldap proxies/db servers
tls_reqcert never

# CA certificates for server certificate verification
#tls_cacertdir /etc/pki/tls/certs
#tls_cacertfile /etc/pki/tls/root.crt

# Seed the PRNG if /dev/urandom is not provided
#tls_randfile /var/run/egd-pool

# SSL cipher suite
# See man ciphers for syntax
tls_ciphers TLSv1.2

# Client certificate and key
# Use these, if your server requires client authentication.
#tls_cert
#tls_key

referrals off
pagesize 1000
filter passwd (&(objectClass=user)(objectClass=person)(!(objectClass=computer)))
map    passwd uid              sAMAccountName
map    passwd uidNumber        objectSid:S-1-5-21-1853637497-491971987-2917381224
map    passwd gecos            displayName
map    passwd gidNumber        primaryGroupID
map    passwd homeDirectory    "/home/$sAMAccountName"
map    passwd loginShell       "/bin/bash"
filter group  (|(objectClass=group)(objectClass=person))
map    group  gidNumber        objectSid:S-1-5-21-1853637497-491971987-2917381224
 
# This comment prevents repeated auto-migration of settings.
